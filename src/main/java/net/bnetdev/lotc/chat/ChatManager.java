/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.chat;

import java.util.ArrayList;
import net.bnetdev.lotc.LOTC;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatManager {
    
    private LOTC plugin;
    private PaginationUtility pUtility;
    private String prefix = "[Server Framework] ";
    
    public ChatManager(LOTC instance) {
        this.plugin = instance;
        this.pUtility = new PaginationUtility(instance);
    }
    
    public void init() {
        this.prefix = this.plugin.getFileManager().getConfiguration().getString("Plugin Chat Tag", "Tag").concat(" ");
    }
    
    public void log(String message) {
        this.plugin.getLogger().info(message);
    }
    
    public void broadcast(String message) {
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.prefix.concat(message)));
    }
    
    public void broadcast(Player p, String message) {
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.prefix.concat(message)));
    }
    
    public String getPrefix() {
        return this.prefix;
    }
    
    public void paginate(Player p, ArrayList<String> list, int page, int countAll) {
        this.pUtility.paginate(p, list, page, countAll);
    }
    
}
