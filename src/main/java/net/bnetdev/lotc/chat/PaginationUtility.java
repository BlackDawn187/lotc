/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.chat;

import java.util.ArrayList;
import net.bnetdev.lotc.LOTC;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class PaginationUtility {

    private LOTC plugin;

    private int contentLinesPerPage = 10;

    protected PaginationUtility(LOTC instance) {
        this.plugin = instance;
    }

    /**
     * Sends the HELP as a paginated list of strings in chat to a player
     *
     * @param sender The sender to send the list to
     * @param list The list to paginate
     * @param page The page number to display.
     * @param countAll The count of all available entries
     */
    protected void paginate(Player sender, ArrayList<String> list, int page, int countAll) {
        int totalPageCount = 1;

        if ((list.size() % contentLinesPerPage) == 0) {
            if (list.size() > 0) {
                totalPageCount = list.size() / contentLinesPerPage;
            }
        } else {
            totalPageCount = (list.size() / contentLinesPerPage) + 1;
        }

        if (page <= totalPageCount) {
            sender.sendMessage(ChatColor.WHITE + "-------------- " + ChatColor.translateAlternateColorCodes('&', this.plugin.getChatManager().getPrefix()) + "" + ChatColor.WHITE + " --------------");
            sender.sendMessage(ChatColor.GREEN + " Viewing Page (" + String.valueOf(page) + "/" + totalPageCount + ")");
            sender.sendMessage(ChatColor.WHITE + "----------------------------------------");

            if (list.isEmpty()) {
                sender.sendMessage(this.plugin.getChatManager().getPrefix().concat("The page you requested does not exist."));
            } else {
                int i = 0, k = 0;
                page--;

                for (String entry : list) {
                    k++;
                    if ((((page * contentLinesPerPage) + i + 1) == k) && (k != ((page * contentLinesPerPage) + contentLinesPerPage + 1))) {
                        i++;
                        sender.sendMessage(entry);
                    }
                }
            }

            /* Fix "Footer" when command manager is in place
            sender.sendMessage(ChatColor.WHITE + "----------------------------------------");
            
            if (totalPageCount > 1) {
                if (!plugin.getSettingsManager().getAliasMode()) {
                    sender.sendMessage(ChatColor.GREEN + " Type: '/bc cmd #' to view additional pages.");
                } else if (plugin.getSettingsManager().getAliasMode()) {
                    sender.sendMessage(ChatColor.GREEN + " Type: '/" + plugin.getSettingsManager().getAlias() + " cmd #' to view additional pages.");
                }
                sender.sendMessage(ChatColor.WHITE + "----------------------------------------");
            }
            */
        } else {
            sender.sendMessage(this.plugin.getChatManager().getPrefix().concat(ChatColor.translateAlternateColorCodes('&', ChatColor.WHITE + "There is only " + totalPageCount + " page(s)!")));
        }
    }
}
