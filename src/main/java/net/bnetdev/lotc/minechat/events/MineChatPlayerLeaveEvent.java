/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.minechat.events;

import net.bnetdev.lotc.minechat.ChatConnection;
import org.bukkit.entity.Player;

public class MineChatPlayerLeaveEvent extends MineChatEvent {

    private final Player player;
    private final ChatConnection conn;

    public MineChatPlayerLeaveEvent(final Player player, final ChatConnection conn) {
        this.player = player;
        this.conn = conn;
    }

    public Player getPlayer() {
        return this.player;
    }

    public ChatConnection getConnection() {
        return this.conn;
    }
}
