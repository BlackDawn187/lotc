/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.minechat;

import net.bnetdev.lotc.LOTC;
import org.bukkit.entity.Player;

public class ChatConnection {

    private final Player player;
    private final DeviceType type;

    public ChatConnection(Player player, DeviceType type) {
        this.player = player;
        this.type = type;
    }

    public void close(String clientMsg) {
        if (LOTC.getInstance().getMineChatManager().usingMineChat(player)) {
            LOTC.getInstance().getMineChatManager().removeClient(player.getUniqueId(), this);
            player.kickPlayer(clientMsg);
        }
    }

    public Player getPlayer() {
        return this.player;
    }

    public DeviceType getDeviceType() {
        return this.type;
    }
}
