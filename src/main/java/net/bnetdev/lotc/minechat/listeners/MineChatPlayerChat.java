/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.minechat.listeners;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.files.Configuration;
import net.bnetdev.lotc.minechat.DeviceType;
import net.bnetdev.lotc.minechat.ChatConnection;
import net.bnetdev.lotc.minechat.events.MineChatPlayerJoinEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class MineChatPlayerChat implements Listener {

    private LOTC plugin;

    public MineChatPlayerChat(LOTC instance) {
        this.plugin = instance;
    }

    @EventHandler
    public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
        if (e.getMessage().toLowerCase().contains("minechat") && e.getMessage().contains("connected")) {
            Configuration configuration = this.plugin.getFileManager().getConfiguration();

            DeviceType type;
            if (e.getMessage().toLowerCase().contains("iphone")) {
                type = DeviceType.IPHONE;
                if (configuration.getBoolean("MineChat.IOS", "BlockMessages")) {
                    e.setCancelled(true);
                }
            } else if (e.getMessage().toLowerCase().contains("ipad")) {
                type = DeviceType.IPAD;
                if (configuration.getBoolean("MineChat.IOS", "BlockMessages")) {
                    e.setCancelled(true);
                }
            } else if (e.getMessage().toLowerCase().contains("android")) {
                type = DeviceType.ANDROID;
                if (configuration.getBoolean("MineChat.ANDROID", "BlockMessages")) {
                    e.setCancelled(true);
                }
            } else {
                type = DeviceType.UNKNOWN;
                if (configuration.getBoolean("MineChat.UNKNOWN", "BlockMessages")) {
                    e.setCancelled(true);
                }
            }

            Bukkit.getServer().getPluginManager().callEvent(new MineChatPlayerJoinEvent(e.getPlayer(), new ChatConnection(e.getPlayer(), type)));
        }
    }

}
