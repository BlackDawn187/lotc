/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.minechat.listeners;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.minechat.events.MineChatPlayerLeaveEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class MineChatPlayerLeave implements Listener {

    private LOTC plugin;
    
    public MineChatPlayerLeave(LOTC instance) {
        this.plugin = instance;
    }
    
    @EventHandler
    public void onMineChatClientLeave(MineChatPlayerLeaveEvent e) {
        if (!e.isCancelled()) {
            this.plugin.getMineChatManager().removeClient(e.getPlayer().getUniqueId(), e.getConnection());
        }
    }
}
