/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.minechat.listeners;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.files.Configuration;
import net.bnetdev.lotc.minechat.DeviceType;
import net.bnetdev.lotc.minechat.events.MineChatPlayerJoinEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class MineChatPlayerJoin implements Listener {

    private LOTC plugin;

    public MineChatPlayerJoin(LOTC instance) {
        this.plugin = instance;
    }

    @EventHandler
    public void onMineChatPlayerJoin(final MineChatPlayerJoinEvent e) {
        if (!e.isCancelled()) {
            final Configuration configuration = this.plugin.getFileManager().getConfiguration();
            if (e.getPlayer().hasPlayedBefore()) {
                if (e.getConnection().getDeviceType() == DeviceType.IPHONE || e.getConnection().getDeviceType() == DeviceType.IPAD) {
                    if (!configuration.getBoolean("MineChat.IOS", "Permitted")) {
                        final Player p = e.getPlayer();
                        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                            @Override
                            public void run() {
                                p.kickPlayer(configuration.getString("MineChat.IOS", "NotPermittedMessage"));
                            }
                        });
                        e.setCancelled(true);
                    } else if (this.plugin.getFileManager().getConfiguration().getBoolean("MineChat.IOS", "SendDiscouragementMessage")) {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.IOS", "DiscouragementMessage"));
                    } else {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.IOS", "RejoinMessage").replace("%player%", e.getPlayer().getName()));
                    }
                } else if (e.getConnection().getDeviceType() == DeviceType.ANDROID) {
                    if (!configuration.getBoolean("MineChat.ANDROID", "Permitted")) {
                        final Player p = e.getPlayer();
                        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                            @Override
                            public void run() {
                                p.kickPlayer(configuration.getString("MineChat.ANDROID", "NotPermittedMessage"));
                            }
                        });
                        e.setCancelled(true);
                    } else if (this.plugin.getFileManager().getConfiguration().getBoolean("MineChat.ANDROID", "SendDiscouragementMessage")) {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.ANDROID", "DiscouragementMessage"));
                    } else {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.ANDROID", "RejoinMessage").replace("%player%", e.getPlayer().getName()));
                    }
                } else if (e.getConnection().getDeviceType() == DeviceType.UNKNOWN) {
                    if (!configuration.getBoolean("MineChat.UNKNOWN", "Permitted")) {
                        final Player p = e.getPlayer();
                        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                            @Override
                            public void run() {
                                p.kickPlayer(configuration.getString("MineChat.UNKNOWN", "NotPermittedMessage"));
                            }
                        });
                        e.setCancelled(true);
                    } else if (this.plugin.getFileManager().getConfiguration().getBoolean("MineChat.UNKNOWN", "SendDiscouragementMessage")) {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.UNKNOWN", "DiscouragementMessage"));
                    } else {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.UNKNOWN", "RejoinMessage").replace("%player%", e.getPlayer().getName()));
                    }
                }
            } else {
                if (e.getConnection().getDeviceType() == DeviceType.IPHONE || e.getConnection().getDeviceType() == DeviceType.IPAD) {
                    if (!configuration.getBoolean("MineChat.IOS", "Permitted")) {
                        final Player p = e.getPlayer();
                        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                            @Override
                            public void run() {
                                p.kickPlayer(configuration.getString("MineChat.IOS", "NotPermittedMessage"));
                            }
                        });
                        e.setCancelled(true);
                    } else if (this.plugin.getFileManager().getConfiguration().getBoolean("MineChat.IOS", "SendDiscouragementMessage")) {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.IOS", "DiscouragementMessage"));
                    }
                } else if (e.getConnection().getDeviceType() == DeviceType.ANDROID) {
                    if (!configuration.getBoolean("MineChat.ANDROID", "Permitted")) {
                        final Player p = e.getPlayer();
                        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                            @Override
                            public void run() {
                                p.kickPlayer(configuration.getString("MineChat.ANDROID", "NotPermittedMessage"));
                            }
                        });
                        e.setCancelled(true);
                    } else if (this.plugin.getFileManager().getConfiguration().getBoolean("MineChat.ANDROID", "SendDiscouragementMessage")) {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.ANDROID", "DiscouragementMessage"));
                    }
                } else if (e.getConnection().getDeviceType() == DeviceType.UNKNOWN) {
                    if (!configuration.getBoolean("MineChat.UNKNOWN", "Permitted")) {
                        final Player p = e.getPlayer();
                        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                            @Override
                            public void run() {
                                p.kickPlayer(configuration.getString("MineChat.UNKNOWN", "NotPermittedMessage"));
                            }
                        });
                        e.setCancelled(true);
                    } else if (this.plugin.getFileManager().getConfiguration().getBoolean("MineChat.UNKNOWN", "SendDiscouragementMessage")) {
                        this.plugin.getChatManager().broadcast(e.getPlayer(), this.plugin.getFileManager().getConfiguration().getString("MineChat.UNKNOWN", "DiscouragementMessage"));
                    }
                }
            }

            this.plugin.getMineChatManager().addClient(e.getPlayer().getUniqueId(), e.getConnection());
        }
    }
}
