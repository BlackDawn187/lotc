/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.minechat;

import java.util.HashMap;
import java.util.UUID;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.files.Configuration;
import org.bukkit.entity.Player;

public class MineChatManager {

    private LOTC plugin;
    private Configuration configuration;
    private HashMap<UUID, ChatConnection> clients = new HashMap<>();

    public MineChatManager(LOTC instance) {
        this.plugin = instance;
        this.configuration = this.plugin.getFileManager().getConfiguration();
        
        
    }

    /**
     * Start keeping track of a client by UUID. You must add a client to be able
     * to check if they're using a MineChat client.
     *
     * @param id UUID
     * @param conn ChatConnection
     */
    public void addClient(UUID id, ChatConnection conn) {
        if (conn == null || id == null) {
            return;
        }
        this.clients.put(id, conn);
    }

    /**
     * Stop keeping track of a client. This should be used when you're done
     * checking if a client is using MineChat.
     *
     * @param id UUID
     * @param conn ChatConnection
     */
    public void removeClient(UUID id, ChatConnection conn) {
        if (conn == null || id == null) {
            return;
        }
        this.clients.remove(id, conn);
    }

    /**
     * Sees if there's an entry for a UUID.
     *
     * @param UUID id
     * @return boolean True if a client exists with that UUID. False if a client
     * doesn't exist with that UUID.
     */
    private boolean hasClientByUUID(UUID id) {
        return this.clients.containsKey(id);
    }

    /**
     * Returns the ChatConnection of a client from UUID.
     *
     * @param UUID id
     * @return ChatConnection, if client found, and null if UUID not in HashMap.
     */
    private ChatConnection getConnectionByUUID(UUID id) {
        if (hasClientByUUID(id)) {
            return this.clients.get(id);
        }
        return null;
    }

    /**
     * Checks if a player is using MineChat.
     *
     * @param player Player
     * @return boolean
     */
    public boolean usingMineChat(Player player) {
        return this.hasClientByUUID(player.getUniqueId());
    }

    /**
     * Checks if a UUID is using MineChat
     *
     * @param id UUID
     * @return boolean
     */
    public boolean usingMineChat(UUID id) {
        return this.hasClientByUUID(id);
    }

    /**
     * Gets a player's ChatConnection object if one exists.
     *
     * @param player Player
     * @return ChatConnection if one exists or null if one doesn't.
     */
    public ChatConnection getConnection(Player player) {
        return this.getConnectionByUUID(player.getUniqueId());
    }

    /**
     * Returns a HashMap of all the clients the manager is keeping track of.
     *
     * @return HashMap<UUID, ChatConnection>
     */
    public HashMap<UUID, ChatConnection> getClients() {
        return this.clients;
    }

}
