/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.listeners;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.commands.AbstractCommand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandPreprocess implements Listener {

    private LOTC plugin;

    public CommandPreprocess(LOTC instance) {
        this.plugin = instance;
    }

    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().contains("/")) {
            if (!event.getMessage().contains(" ")) {
                if (this.plugin.getCommandManager().exists(event.getMessage().replace("/", ""))) {
                    AbstractCommand c = this.plugin.getCommandManager().get(event.getMessage().replace("/", ""));
                    c.execute(event.getPlayer(), "");
                    event.setCancelled(true);
                }
            } else {
                String temp[] = event.getMessage().split("\\s+");
                temp[0] = temp[0].replace("/", "");

                if (this.plugin.getCommandManager().exists(temp[0])) {
                    if (temp.length == 2) {
                        AbstractCommand c = this.plugin.getCommandManager().get(temp[0].replace("/", ""));
                        c.execute(event.getPlayer(), temp[1]);
                    } else if (temp.length == 3) {
                        AbstractCommand c = this.plugin.getCommandManager().get(temp[0].replace("/", ""));
                        c.execute(event.getPlayer(), temp[1], temp[2]);
                    }
                }
            }
        }
    }

}
