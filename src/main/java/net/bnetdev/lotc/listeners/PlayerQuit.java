/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.listeners;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.minechat.ChatConnection;
import net.bnetdev.lotc.minechat.events.MineChatPlayerLeaveEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {

    private LOTC plugin;

    public PlayerQuit(LOTC instance) {
        this.plugin = instance;
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        if (this.plugin.getMineChatManager().usingMineChat(e.getPlayer())) {
            final ChatConnection conn = this.plugin.getMineChatManager().getConnection(e.getPlayer());
            this.plugin.getMineChatManager().removeClient(e.getPlayer().getUniqueId(), conn);
            Bukkit.getServer().getPluginManager().callEvent(new MineChatPlayerLeaveEvent(e.getPlayer(), conn));
        }
    }
}
