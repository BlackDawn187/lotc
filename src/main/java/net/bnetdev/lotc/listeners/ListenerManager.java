/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.listeners;

import net.bnetdev.lotc.titles.listeners.FirstPlayerJoinTitle;
import net.bnetdev.lotc.titles.listeners.ReturningPlayerJoinTitle;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.files.Configuration;
import net.bnetdev.lotc.minechat.listeners.MineChatPlayerChat;
import net.bnetdev.lotc.minechat.listeners.MineChatPlayerJoin;
import net.bnetdev.lotc.minechat.listeners.MineChatPlayerLeave;
import org.bukkit.Bukkit;

public class ListenerManager {

    private LOTC plugin;
    private Configuration configuration;

    public ListenerManager(LOTC instance) {
        this.plugin = instance;
        this.configuration = this.plugin.getFileManager().getConfiguration();

        if (this.configuration.getBoolean("Title.FirstJoin", "Enabled")) {
            Bukkit.getPluginManager().registerEvents(new FirstPlayerJoinTitle(this.plugin), this.plugin);
        }

        if (this.configuration.getBoolean("Title.Rejoin", "Enabled")) {
            Bukkit.getPluginManager().registerEvents(new ReturningPlayerJoinTitle(this.plugin), this.plugin);
        }

        Bukkit.getPluginManager().registerEvents(new MineChatPlayerChat(this.plugin), this.plugin);
        Bukkit.getPluginManager().registerEvents(new MineChatPlayerJoin(this.plugin), plugin);
        Bukkit.getPluginManager().registerEvents(new MineChatPlayerLeave(this.plugin), plugin);
        
        
        Bukkit.getPluginManager().registerEvents(new PlayerQuit(this.plugin), this.plugin);
        Bukkit.getPluginManager().registerEvents(new CommandPreprocess(this.plugin), plugin);
    }

}
