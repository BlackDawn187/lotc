/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.economy;

import net.bnetdev.lotc.LOTC;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class EconomyManager {

    private LOTC plugin;
    private Vault v;
    private Economy e;

    private boolean isVaultAvailable = false, isEconomyAvailable = false;

    public EconomyManager(LOTC instance) {
        this.plugin = instance;
        this.init();
    }

    private void init() {
        if (Bukkit.getPluginManager().getPlugin("Vault") != null) {
            this.v = (Vault) Bukkit.getPluginManager().getPlugin("Vault");
            this.isVaultAvailable = true;

            RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
            if (rsp != null) {
                this.e = rsp.getProvider();
                this.isEconomyAvailable = true;
            }
        }
    }

    public boolean isEconomyEnabled() {
        return this.isEconomyAvailable;
    }
    
    public boolean isVaultEnabled() {
        return this.isVaultAvailable;
    }
    
    public boolean hasBalance(Player p, double amount) {
        return this.getBalance(p) >= amount;
    }

    public double getBalance(Player p) {
        return this.e.getBalance(p);
    }

    public String format(double value) {
        return this.e.format(value);
    }

    public EconomyResponse withdraw(Player p, double amount) {
        if (this.getBalance(p) >= amount) {
            return this.e.withdrawPlayer(p, amount);
        }
        
        throw new NullPointerException("The specified player does not have an adequate balance to withdraw the required funds.\n " +
                                       "Please verify the player has an adequate balance by using the getBalance method.");
    }

}
