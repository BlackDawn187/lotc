/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.commands;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class AbstractCommand {
    
    private final JavaPlugin plugin;
    private final String name;
    private String permission, usage;
    
    public AbstractCommand(JavaPlugin instance, String instance2) {
        this.plugin = instance;
        this.name = instance2;
    }
    
    public String getUsage() {
        return this.usage;
    }
    
    public void setUsage(String usage) {
        this.usage = usage;
    }
    
    public String getPermission() {
        return this.permission;
    }
    
    public void setPermission(String permission) {
        this.permission = permission;
    }
    
    public String getName() {
        return this.name;
    }
    
    public JavaPlugin getPlugin() {
        return this.plugin;
    }
    
    public abstract void execute(Player p, String var);
    
    public abstract void execute(Player p, String var, String var2);
}
