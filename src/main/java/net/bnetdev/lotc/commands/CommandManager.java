/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.commands;

import java.util.ArrayList;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.extensions.Extension;

public class CommandManager {

    private LOTC plugin;

    private ArrayList<AbstractCommand> commands = new ArrayList<>();

    public CommandManager(LOTC instance) {
        this.plugin = instance;
    }

    public void init() {

    }

    public void add(Extension e, AbstractCommand command) {
        if (!this.commands.contains(command)) {
            this.commands.add(command);
            e.hookMessage(command.getName().concat(" registered with Command Manager."));
            e.pluginMessage("Successfully added ".concat(command.getName()).concat(" to ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
        } else {
            e.hookMessage(command.getName().concat(" is already registered."));
            e.pluginMessage("Failed to add ".concat(command.getName()).concat(" to ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
        }
    }

    public void add(Extension e, AbstractCommand[] commands) {
        for (int i = 0; i < commands.length; i++) {
            if (!this.commands.contains(commands[i])) {
                this.commands.add(commands[i]);
                e.hookMessage(commands[i].getName().concat(" registered with Command Manager."));
                e.pluginMessage("Successfully added ".concat(commands[i].getName()).concat(" to ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
            } else {
                e.hookMessage(commands[i].getName().concat(" is already registered."));
                e.pluginMessage("Failed to add ".concat(commands[i].getName()).concat(" to ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
            }
        }
    }

    public void remove(Extension e, AbstractCommand command) {
        if (this.commands.contains(command)) {
            this.commands.remove(command);
            e.hookMessage(command.getName().concat(" unregistered with Command Manager."));
            e.pluginMessage("Successfully unregistered ".concat(command.getName()).concat(" from ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
        } else {
            e.hookMessage(command.getName().concat(" is not registered with Command Manager."));
            e.pluginMessage(command.getName().concat(" is not registered with ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
        }
    }

    public void remove(Extension e, AbstractCommand[] commands) {
        for (int i = 0; i < commands.length; i++) {
            if (this.commands.contains(commands[i])) {
                this.commands.remove(commands[i]);
                e.hookMessage(commands[i].getName().concat(" unregistered with Command Manager."));
                e.pluginMessage("Successfully removed ".concat(commands[i].getName()).concat(" from ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
            } else {
                e.hookMessage(commands[i].getName().concat(" is not registered with Command Manager."));
                e.pluginMessage(commands[i].getName().concat(" is not registered with ").concat(this.plugin.getChatManager().getPrefix()).concat(" Command Manager."));
            }
        }
    }

    public boolean exists(String name) {
        for (AbstractCommand c : this.commands) {
            if (c.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }

        return false;
    }

    public AbstractCommand get(String name) {
        for (AbstractCommand c : this.commands) {
            if (c.getName().equalsIgnoreCase(name)) {
                return c;
            }
        }

        throw new NullPointerException("The command requested does not exist. Please use CommandManager.exists(Command Name) prior to getting a command.");
    }

}
