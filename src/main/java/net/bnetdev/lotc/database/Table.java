/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.database;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bnetdev.lotc.LOTC;
import org.bukkit.Bukkit;

public class Table {

    private final LOTC plugin;
    private final String name;
    private final String[] sql;

    public Table(LOTC instance, String name, String... sql) {
        this.plugin = instance;
        this.name = name;
        this.sql = sql;
        this.init();
    }

    private boolean init() {
        final AtomicBoolean result = new AtomicBoolean();
        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, new Runnable() {

            @Override
            public void run() {
                if (plugin.getDatabaseManager().getDatabase().getType() == DatabaseType.MySQL) {
                    for (String statement : sql) {
                        try {
                            plugin.getDatabaseManager().getDatabase().getConnection().prepareStatement(statement).executeUpdate();
                            result.set(true);
                        } catch (SQLException ex) {
                            Logger.getLogger(Table.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else if (plugin.getDatabaseManager().getDatabase().getType() == DatabaseType.SQLite) {
                    for (String statement : sql) {
                        try {
                            plugin.getDatabaseManager().getDatabase().getConnection().prepareStatement(statement).executeUpdate();
                            result.set(true);
                        } catch (SQLException ex) {
                            Logger.getLogger(Table.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

        });
        
        return result.get();
    }

}
