/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.database;

import java.util.ArrayList;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.extensions.Extension;
import net.bnetdev.lotc.files.Configuration;

public class DatabaseManager {

    private LOTC plugin;
    private Extension e;
    private Configuration config;
    private Database database;
    private ArrayList<Table> tables = new ArrayList<>();

    public DatabaseManager(LOTC instance) {
        this.plugin = instance;
        this.config = instance.getFileManager().getConfiguration();
        
        String type = this.config.getString("Database", "Type");
        if (type.equalsIgnoreCase("mysql")) {
            String[] args = {
                this.config.getString("Database.MySQL", "IP"),
                this.config.getString("Database.MySQL", "Port"),
                this.config.getString("Database.MySQL", "Database"),
                this.config.getString("Database.MySQL", "Username"),
                this.config.getString("Database.MySQL", "Password"),
                this.config.getString("Database.MySQL.Pooling", "MinimumConnections"),
                this.config.getString("Database.MySQL.Pooling", "MaximumConnections"),
                this.config.getString("Database.MySQL.Pooling", "ConnectionTimeout")
            };
            this.database = new Database(this.plugin, DatabaseType.MySQL, args);
        } else if (type.equalsIgnoreCase("sqlite")) {
            String[] args = {
                "127.0.0.1",
                "3306",
                this.config.getString("Database.SQLite", "Database"),
                "test",
                "test",
                "0",
                "0",
                "0"};
            this.database = new Database(this.plugin, DatabaseType.SQLite, args);
        } else {
            throw new NullPointerException("Invalid database type specified in the Configuration.ini file.");
        }
    }

    public DatabaseManager(Extension instance, DatabaseType type, String[] args) {
        this.e = instance;
        if (type == DatabaseType.MySQL) {
            this.database = new Database(this.e, type, args);
        } else if (type == DatabaseType.SQLite) {
            this.database = new Database(this.e, type, args);
        } else {
            throw new NullPointerException("Invalid database type specified.");
        }
    }

    public Database getDatabase() {
        return this.database;
    }

}
