/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.files.Configuration;
import org.bukkit.plugin.java.JavaPlugin;

public class Database {

    private ArrayList<Table> tables = new ArrayList<>();

    private final DatabaseType TYPE;
    private String HOSTNAME;
    private String PORT;
    private final String DATABASE;
    private String USERNAME;
    private String PASSWORD;
    private final int MINCONNECTIONS;
    private final int MAXCONNECTIONS;
    private final long CONNTIMEOUT;

    private HikariDataSource ds;

    public Database(Object caller, DatabaseType type, String[] args) {
        this.TYPE = type;
        this.HOSTNAME = args[0];
        this.PORT = args[1];
        this.DATABASE = args[2];
        this.USERNAME = args[3];
        this.PASSWORD = args[4];
        this.MINCONNECTIONS = Integer.valueOf(args[5]);
        this.MAXCONNECTIONS = Integer.valueOf(args[6]);
        this.CONNTIMEOUT = Long.valueOf(args[7]);

        this.init();

        if (caller instanceof JavaPlugin) {
            if (((JavaPlugin) caller).getDescription().getName().equalsIgnoreCase("LOTC")) {
                this.load();
            }
        }
    }

    private void init() {
        HikariConfig config = new HikariConfig();

        switch (this.TYPE) {
            case MySQL:
                config.setJdbcUrl("jdbc:mysql://".concat(this.HOSTNAME).concat(":").concat(this.PORT).concat("/").concat(this.DATABASE));
                config.setDriverClassName("com.mysql.jdbc.Driver");
                config.setUsername(this.USERNAME);
                config.setPassword(this.PASSWORD);
                config.setMinimumIdle(this.MINCONNECTIONS);
                config.setMaximumPoolSize(this.MAXCONNECTIONS);
                config.setConnectionTimeout(this.CONNTIMEOUT);
                break;
            case SQLite:
                config.setJdbcUrl("jdbc:sqlite://".concat(this.DATABASE));
                config.setDriverClassName("org.sqlite.JDBC");
                config.setConnectionTestQuery("SELECT 1");
                break;
        }

        this.ds = new HikariDataSource(config);
    }

    private void load() {
        ArrayList<Configuration> configurations = new ArrayList<>();
        configurations.add(LOTC.getInstance().getFileManager().getConfiguration("Internal".concat(File.separator).concat("SQLTables"), "Players.ini"));

        for (Configuration c : configurations) {
            LOTC.getInstance().getChatManager().log("Loading SQL Table: ".concat(c.getString("Configuration", "Name")));
            Table temp = new Table(LOTC.getInstance(), c.getString("Configuration", "Name"), c.getStringArray("Statements", "Statement"));
            LOTC.getInstance().getChatManager().log("Loaded SQL Table: ".concat(c.getString("Configuration", "Name")));
        }

    }

    public void load(String s) {

    }

    private HikariDataSource getDataSource() {
        return this.ds;
    }

    public Connection getConnection() {
        try {
            return this.getDataSource().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        throw new NullPointerException("Unable to acquire Connection.");
    }

    public DatabaseType getType() {
        return this.TYPE;
    }
}
