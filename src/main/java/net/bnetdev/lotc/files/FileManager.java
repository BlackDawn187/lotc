/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.files;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.extensions.Extension;
import org.bukkit.plugin.java.JavaPlugin;

public class FileManager {

    private LOTC plugin;
    private File ext, internal;
    private Configuration configuration;

    public FileManager(LOTC instance) {
        this.plugin = instance;
        this.init();
    }

    public Configuration getConfiguration() {
        return this.configuration;
    }

    public Configuration getConfiguration(String directory, String name) {
        File f = new File(this.plugin.getDataFolder().toString().concat(File.separator).concat(directory).concat(File.separator).concat(name));
        if (!f.exists()) {
            try {
                f.createNewFile();
                return new Configuration(this.plugin, this.plugin.getResource(name), this.plugin.getDataFolder().toString().concat(File.separator).concat(directory).concat(File.separator).concat(name));
            } catch (IOException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                return new Configuration(this.plugin, f);
            }
        } else {
            return new Configuration(this.plugin, f);
        }
    }

    public Configuration getConfiguration(Extension e, String name) throws IOException {
        if (e.isRegistered()) {
            if (!e.getDataFolder().exists()) {
                e.getDataFolder().mkdir();
            }

            File f = new File(this.plugin.getDataFolder().getAbsolutePath().concat(File.separator).concat("Extensions").concat(File.separator).concat(e.getName()).concat(File.separator).concat(name));
            if (!f.exists()) {
                try {
                    f.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            return new Configuration((JavaPlugin) e.getInstance(), ((JavaPlugin) e.getInstance()).getResource(name), this.plugin.getDataFolder().getAbsolutePath().concat(File.separator).concat("Extensions").concat(File.separator).concat(e.getName()).concat(File.separator).concat(name));
        } else {
            this.plugin.getChatManager().log("Failed to retrieve configuration for unregistered extension: ".concat(e.getName()));
            throw new NullPointerException();
        }
    }

    private void init() {
        if (!this.plugin.getDataFolder().exists()) {
            this.plugin.getDataFolder().mkdir();
        }

        this.ext = new File(this.plugin.getDataFolder().toString().concat(File.separator.concat("Extensions")));
        if (!this.ext.exists()) {
            this.ext.mkdir();
        }

        this.internal = new File(this.plugin.getDataFolder().toString().concat(File.separator.concat("Internal")));
        if (!this.internal.exists()) {
            this.internal.mkdir();
            
            File sqlTables = new File(this.internal.toString().concat(File.separator).concat("SQLTables"));
            if (!sqlTables.exists()) {
                sqlTables.mkdir();
            }
        }

        File f = new File(this.plugin.getDataFolder().toString().concat(File.separator).concat("Configuration.ini"));
        if (!f.exists()) {
            try {
                f.createNewFile();
                this.configuration = new Configuration(this.plugin, this.plugin.getResource("Configuration.ini"), this.plugin.getDataFolder().toString().concat(File.separator).concat("Configuration.ini"));
            } catch (IOException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            this.configuration = new Configuration(this.plugin, f);
        }
    }

}
