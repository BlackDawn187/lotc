/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Ini;

public abstract class IniFile {

    private File f;
    private Ini ini;

    public IniFile(File file) {
        this.f = file;
    }

    public IniFile(InputStream is, String file) {
        this.f = new File(file);
        if (!this.f.exists()) {
            try {
                this.f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(IniFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            FileOutputStream fos = new FileOutputStream(this.f);

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = is.read(bytes)) != -1) {
                fos.write(bytes, 0, read);
            }

            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IniFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IniFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        init();
    }

    public final void init() {
        this.ini = new Ini();
        try {
            this.ini.load(this.f);
        } catch (IOException ex) {
            Logger.getLogger(IniFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public File getFile() {
        return this.f;
    }

    public boolean isEmpty() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(this.f.getAbsolutePath()));
        return br.readLine() == null;
    }

    public boolean remove(String section, String key) {
        this.ini.remove(section, key);
        return this.save();
    }

    private void set(String section, String key, String value) {
        if (this.ini.get(section) == null) {
            this.ini.add(section);
        }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    private void set(String section, String key, int value) {
        if (this.ini.get(section) == null) {
            this.ini.add(section);
        }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    private void set(String section, String key, long value) {
        if (this.ini.get(section) == null) {
            this.ini.add(section);
        }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    private void set(String section, String key, boolean value) {
        if (this.ini.get(section) == null) {
            this.ini.add(section);
        }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    public boolean setString(String section, String key, String value) {
        this.set(section, key, value);
        return this.save();
    }

    public String getString(String section, String key) {
        return this.ini.get(section).get(key);
    }

    public boolean setInt(String section, String key, int value) {
        this.set(section, key, value);
        return this.save();
    }

    public int getInt(String section, String key) {
        return Integer.valueOf(this.ini.get(section).get(key));
    }

    public boolean setLong(String section, String key, long value) {
        this.set(section, key, value);
        return this.save();
    }

    public long getLong(String section, String key) {
        return Long.valueOf(this.ini.get(section).get(key));
    }

    public boolean setBoolean(String section, String key, boolean value) {
        this.set(section, key, value);
        return this.save();
    }

    public boolean getBoolean(String section, String key) {
        return Boolean.valueOf(this.ini.get(section).get(key));
    }

    public String[] getStringArray(String section, String key) {
        return this.ini.get(section).getAll((key), String[].class);
    }

    public int[] getIntArray(String section, String key) {
        return this.ini.get(section).getAll((key), int[].class);
    }

    public long[] getLongArray(String section, String key) {
        return this.ini.get(section).getAll((key), long[].class);
    }

    public boolean[] getBooleanArray(String section, String key) {
        return this.ini.get(section).getAll((key), boolean[].class);
    }

    public boolean addSection(String name) {
        this.ini.add(name);
        return this.save();
    }
    
    public String[] getKeys(String section) {
        return this.ini.get(section).keySet().toArray(new String[0]);
    }

    public Set<String> getSections() {
        return this.ini.keySet();
    }

    public void setDefaults(InputStream stream) {
        try {
            Files.copy(stream, Paths.get(this.getFile().getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean save() {
        try {
            this.ini.store(this.f);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(IniFile.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
