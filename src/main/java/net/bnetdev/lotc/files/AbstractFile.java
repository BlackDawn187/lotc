/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Ini;

public abstract class AbstractFile {

    private File file;
    private String name;
    private Ini ini;
    private boolean isLoaded = false;

    public AbstractFile(File file) {
        this.file = file;
    }

    public AbstractFile(InputStream iStream, String file) throws FileNotFoundException, IOException {
        this.file = new File(file);
        if (!this.file.exists()) {
            try {
                this.file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(AbstractFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            FileOutputStream oStream = new FileOutputStream(this.file);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = iStream.read(bytes)) != -1) {
                oStream.write(bytes, 0, read);
            }

            oStream.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(AbstractFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AbstractFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void init() {
       this.ini = new Ini();
        try {
            this.ini.load(this.getFile());
        } catch (IOException ex) {
            Logger.getLogger(AbstractFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public File getFile() {
        return this.file;
    }

    public boolean isEmpty() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(this.file));
        return br.readLine() == null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setLoaded(boolean value) {
        this.isLoaded = value;
    }

    public boolean isLoaded() {
        return this.isLoaded;
    }

    public void setINI(Ini ini) {
        this.ini = ini;
    }

    public Ini getINI() {
        return this.ini;
    }

    public boolean remove(String section, String key) {
        this.ini.remove(section, name);
        return this.save();
    }
    
    private void set(String section, String key, String value) {
        if (this.ini.get(section) == null) { this.ini.add(section); }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    private void set(String section, String key, int value) {
        if (this.ini.get(section) == null) { this.ini.add(section); }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    private void set(String section, String key, long value) {
        if (this.ini.get(section) == null) { this.ini.add(section); }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    private void set(String section, String key, boolean value) {
        if (this.ini.get(section) == null) { this.ini.add(section); }
        if (this.ini.get(section).get(key) == null) {
            this.ini.get(section).add(key, value);
        } else {
            this.ini.get(section).put(key, value);
        }
    }

    public boolean setString(String section, String key, String value) {
        this.set(section, key, value);
        return this.save();
    }

    public String getString(String section, String key) {
        return this.ini.get(section).get(key);
    }

    public boolean setInt(String section, String key, int value) {
        this.set(section, key, value);
        return this.save();
    }

    public int getInt(String section, String key) {
        return Integer.valueOf(this.ini.get(section).get(key));
    }

    public boolean setLong(String section, String key, long value) {
        this.set(section, key, value);
        return this.save();
    }

    public long getLong(String section, String key) {
        return Long.valueOf(this.ini.get(section).get(key));
    }

    public boolean setBoolean(String section, String key, boolean value) {
        this.set(section, key, value);
        return this.save();
    }

    public boolean getBoolean(String section, String key) {
        return Boolean.valueOf(this.ini.get(section).get(key));
    }

    public String[] getStringArray(String section, String key) {
        return this.ini.get(section).getAll((key), String[].class);
    }

    public int[] getIntArray(String section, String key) {
        return this.ini.get(section).getAll((key), int[].class);
    }

    public long[] getLongArray(String section, String key) {
        return this.ini.get(section).getAll((key), long[].class);
    }

    public boolean[] getBooleanArray(String section, String key) {
        return this.ini.get(section).getAll((key), boolean[].class);
    }

    public boolean addSection(String name) {
        this.ini.add(name);
        return this.save();
    }

    public Set<String> getSections() {
        return this.ini.keySet();
    }

    public void setDefaults(InputStream stream) {
        try {
            Files.copy(stream, Paths.get(this.getFile().getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean save() {
        try {
            this.ini.store(this.file);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(AbstractFile.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}