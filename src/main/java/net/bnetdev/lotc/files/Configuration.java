/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bnetdev.lotc.LOTC;
import org.bukkit.plugin.java.JavaPlugin;

public class Configuration extends IniFile {
    
    private LOTC plugin;
    private JavaPlugin jp;
   
    public Configuration(JavaPlugin instance, File file) {
        super(file);
        this.plugin = LOTC.getInstance();
        this.jp = instance;
        this.setDefaults();
        this.init();
    }
    
    public Configuration(JavaPlugin instance, InputStream is, String file) throws IOException {
        super(is, file);
        this.plugin = LOTC.getInstance();
        this.jp = instance;
    }     
    
    public final void setDefaults() {
        try {
            if(this.isEmpty()) {
                Files.copy(jp.getResource(this.getFile().getName()), Paths.get(this.plugin.getExtensionManager().get(jp.getName()).getDataFolder().getAbsolutePath().concat(File.separator)), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }

}
