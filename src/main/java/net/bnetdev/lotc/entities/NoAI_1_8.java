/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.entities;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;

public class NoAI_1_8 {

    public static void freeze(Entity e) {
        net.minecraft.server.v1_8_R3.Entity nmsEN = ((CraftEntity) e).getHandle();
        NBTTagCompound compound = new NBTTagCompound();
        nmsEN.c(compound);
        compound.setByte("NoAI", (byte) 1);
        nmsEN.f(compound);
    }

}
