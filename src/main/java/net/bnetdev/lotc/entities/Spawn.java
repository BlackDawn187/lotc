/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.entities;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Endermite;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Golem;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.Horse;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Pig;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Rabbit;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Shulker;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Squid;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Witch;
import org.bukkit.entity.Wither;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Zombie;

public class Spawn {

    public static Entity spawn(Location l, String name) {
        return (Entity) l.getWorld().spawnEntity(l, EntityType.valueOf(name));
    }

    public static Zombie zombie(Location l) {
        return (Zombie) l.getWorld().spawnEntity(l, EntityType.ZOMBIE);
    }

    public static Skeleton skeleton(Location l) {
        return (Skeleton) l.getWorld().spawnEntity(l, EntityType.SKELETON);
    }

    public static Slime slime(Location l) {
        return (Slime) l.getWorld().spawnEntity(l, EntityType.SLIME);
    }

    public static Spider spider(Location l) {
        return (Spider) l.getWorld().spawnEntity(l, EntityType.SPIDER);
    }

    public static CaveSpider caveSpider(Location l) {
        return (CaveSpider) l.getWorld().spawnEntity(l, EntityType.CAVE_SPIDER);
    }

    public static Bat bat(Location l) {
        return (Bat) l.getWorld().spawnEntity(l, EntityType.BAT);
    }

    public static Creeper creeper(Location l) {
        return (Creeper) l.getWorld().spawnEntity(l, EntityType.CREEPER);
    }

    public static Guardian guardian(Location l) {
        return (Guardian) l.getWorld().spawnEntity(l, EntityType.GUARDIAN);
    }

    public static Witch witch(Location l) {
        return (Witch) l.getWorld().spawnEntity(l, EntityType.WITCH);
    }

    public static Enderman enderman(Location l) {
        return (Enderman) l.getWorld().spawnEntity(l, EntityType.ENDERMAN);
    }

    public static Endermite endermite(Location l) {
        return (Endermite) l.getWorld().spawnEntity(l, EntityType.ENDERMITE);
    }

    public static Blaze blaze(Location l) {
        return (Blaze) l.getWorld().spawnEntity(l, EntityType.BLAZE);
    }

    public static MagmaCube magmaCube(Location l) {
        return (MagmaCube) l.getWorld().spawnEntity(l, EntityType.MAGMA_CUBE);
    }

    public static EnderDragon enderDragon(Location l) {
        return (EnderDragon) l.getWorld().spawnEntity(l, EntityType.ENDER_DRAGON);
    }

    public static Wither wither(Location l) {
        return (Wither) l.getWorld().spawnEntity(l, EntityType.WITHER);
    }

    public static Ghast ghast(Location l) {
        return (Ghast) l.getWorld().spawnEntity(l, EntityType.GHAST);
    }

    public static Silverfish silverfish(Location l) {
        return (Silverfish) l.getWorld().spawnEntity(l, EntityType.SILVERFISH);
    }

    public static IronGolem ironGolem(Location l) {
        return (IronGolem) l.getWorld().spawnEntity(l, EntityType.IRON_GOLEM);
    }

    public static Golem snowMan(Location l) {
        return (Golem) l.getWorld().spawnEntity(l, EntityType.SNOWMAN);
    }

    public static Shulker shulker(Location l) {
        return (Shulker) l.getWorld().spawnEntity(l, EntityType.SHULKER);
    }

    public static Wolf wolf(Location l) {
        return (Wolf) l.getWorld().spawnEntity(l, EntityType.WOLF);
    }

    public static PigZombie pigZombie(Location l) {
        return (PigZombie) l.getWorld().spawnEntity(l, EntityType.PIG_ZOMBIE);
    }

    public static Chicken chicken(Location l) {
        return (Chicken) l.getWorld().spawnEntity(l, EntityType.CHICKEN);
    }

    public static Pig pig(Location l) {
        return (Pig) l.getWorld().spawnEntity(l, EntityType.PIG);
    }

    public static Sheep sheep(Location l) {
        return (Sheep) l.getWorld().spawnEntity(l, EntityType.SHEEP);
    }

    public static Cow cow(Location l) {
        return (Cow) l.getWorld().spawnEntity(l, EntityType.COW);
    }

    public static MushroomCow mushroomCow(Location l) {
        return (MushroomCow) l.getWorld().spawnEntity(l, EntityType.MUSHROOM_COW);
    }

    public static Ocelot ocelot(Location l) {
        return (Ocelot) l.getWorld().spawnEntity(l, EntityType.OCELOT);
    }

    public static Squid squid(Location l) {
        return (Squid) l.getWorld().spawnEntity(l, EntityType.SQUID);
    }

    public static Horse horse(Location l) {
        return (Horse) l.getWorld().spawnEntity(l, EntityType.HORSE);
    }

    public static Rabbit rabbit(Location l) {
        return (Rabbit) l.getWorld().spawnEntity(l, EntityType.RABBIT);
    }

    public static Villager villager(Location l) {
        return (Villager) l.getWorld().spawnEntity(l, EntityType.VILLAGER);
    }

    public static void removeAI(Entity e) {
        String[] version = Bukkit.getVersion().split("\\.");
        switch (version[0]) {
            case "1":
                switch (version[1]) {
                    case "8":
                        NoAI_1_8.freeze(e);
                        break;
//                    case "9":
//                        NoAI_1_9.freeze(e);
//                        break;
                }
                break;
        }
    }

}
