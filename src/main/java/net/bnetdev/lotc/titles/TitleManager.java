/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.titles;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.files.Configuration;

public class TitleManager {

    private LOTC plugin;
    private Configuration configuration;

    private Title firstJoin;
    private Title reJoin;

    public TitleManager(LOTC instance) {
        this.plugin = instance;

        this.configuration = this.plugin.getFileManager().getConfiguration();

        this.firstJoin = new Title(this.configuration.getString("Title.FirstJoin", "Title"), this.configuration.getString("Title.FirstJoin", "SubTitle"));
        this.firstJoin.setTimingsToTicks();
        this.firstJoin.setFadeInTime(this.configuration.getInt("Title.FirstJoin", "FadeInTime"));
        this.firstJoin.setFadeOutTime(this.configuration.getInt("Title.FirstJoin", "FadeOutTime"));
        this.firstJoin.setStayTime(this.configuration.getInt("Title.FirstJoin", "Duration"));

        this.reJoin = new Title(this.configuration.getString("Title.Rejoin", "Title"), this.configuration.getString("Title.Rejoin", "SubTitle"));
        this.reJoin.setTimingsToTicks();
        this.reJoin.setFadeInTime(this.configuration.getInt("Title.Rejoin", "FadeInTime"));
        this.reJoin.setFadeOutTime(this.configuration.getInt("Title.Rejoin", "FadeOutTime"));
        this.reJoin.setStayTime(this.configuration.getInt("Title.Rejoin", "Duration"));

    }
    
    public Title getFirstJoinTitle() {
        return this.firstJoin;
    }
    
    public Title getRejoinTitle() {
        return this.reJoin;
    }
    
    public Title getTitleInstance() {
        return new Title();
    }

}
