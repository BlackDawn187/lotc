/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.titles;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import net.bnetdev.lotc.titles.utils.TitleColorUtil;

public class SendGame {

    /**
     * Send a private message
     *
     * @param message Message to send
     */
    public static void toPlayer(String message, Player player) {
        message = TitleColorUtil.toColors(message); // Color the message
        player.sendMessage(message);
    }

    /**
     * Send a private message
     *
     * @param message Message to send
     * @param permission Permission
     */
    public static void toPermission(String message, String permission) {
        message = TitleColorUtil.toColors(message); // Color the message
        Bukkit.broadcast(message, permission);
    }

    /**
     * Send a broadcast to the server
     *
     * @param message Message to broadcast
     */
    public static void toServer(String message) {
        message = TitleColorUtil.toColors(message); // Color the message
        Bukkit.broadcastMessage(message);
    }

    /**
     * Send a message to a specific world
     *
     * @param message Message
     * @param world World
     */
    public static void toWorld(String message, World world) {
        message = TitleColorUtil.toColors(message); // Color the message
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getWorld().equals(world)) {
                player.sendMessage(message);
            }
        }
    }

    /**
     * Send a message to a specific world
     *
     * @param message Message
     * @param world World
     */
    public static void toWorldGroup(String message, World world) {
        message = TitleColorUtil.toColors(message); // Color the message
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getWorld().equals(world)
                    || player.getWorld().equals(
                            Bukkit.getWorld(world.getName() + "_nether"))
                    || player.getWorld().equals(
                            Bukkit.getWorld(world.getName() + "_the_end"))) {
                player.sendMessage(message);
            }
        }
    }
}
