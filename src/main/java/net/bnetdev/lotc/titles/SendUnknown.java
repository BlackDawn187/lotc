/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.titles;

import org.bukkit.entity.Player;

public class SendUnknown {

    /**
     * Send a message to an unknown receiver
     *
     * @param message Message to send
     * @param sender Receiver
     */
    public static void toSender(String message, Object sender) {
        if (sender instanceof Player) {
            SendGame.toPlayer(message, (Player) sender); // Send to game
        } else {
            SendConsole.message(message); // Send to console
        }
    }
}
