/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.titles.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.entity.Player;

import net.bnetdev.lotc.titles.utils.TitleReflectionUtil.DynamicPackage;

public class TitlePlayerUtil {

    public static int getPing(Player player) {
        int ping = 0;

        try {
            /*
             * first, get the craftPlayer class, for it we use the
             * Class.forName, which will search the class with the given name
             */
            Class<?> craftPlayer = TitleReflectionUtil.getClass(
                    "entity.CraftPlayer", DynamicPackage.CRAFTBUKKIT);

            /*
             * now, convert the player to craftplayer using reflection, this is
             * the same thing as doing ((CraftPlayer)player)
             */
            Object converted = craftPlayer.cast(player);

            /*
             * using the converted class which sould be a CraftPlayer instance,
             * try to find the getHandle method
             */
            Method handle = converted.getClass().getMethod("getHandle");

            /*
             * now we just have to call this method, is the same thing as doing
             * the ((CraftPlayer)player).getHandle();
             */
            Object entityPlayer = handle.invoke(converted);


            /*
             * at this point we should already have the EntityPlayer instance,
             * now we just have to find the 'ping' field
             */
            Field pingField = entityPlayer.getClass().getField("ping");

            /*
             * now that we already have the ping field, just get its value from
             * the entityPlayer object and we are good
             */
            ping = pingField.getInt(entityPlayer);

            return ping;
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | NoSuchFieldException ex) {
            Logger.getLogger(TitlePlayerUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ping;
    }

    public static int getProtocolVersion(Player player) {
        int version = 0;

        try {
            Object handle = TitleReflectionUtil.getHandle(player);
            Object connection = TitleReflectionUtil.getField(handle.getClass(),
                    "playerConnection").get(handle);

            Object networkManager = TitleReflectionUtil.getValue("networkManager",
                    connection);
            version = (Integer) TitleReflectionUtil.getMethod("getVersion",
                    networkManager.getClass()).invoke(networkManager);

            return version;
        } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(TitlePlayerUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(TitlePlayerUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return version;
    }

    public static String getLocale(Player player) {
        String locale = "en_EN";

        try {
            /*
             * first, get the craftPlayer class, for it we use the
             * Class.forName, which will search the class with the given name
             */
            Class<?> craftPlayer = TitleReflectionUtil.getClass(
                    "entity.CraftPlayer", DynamicPackage.CRAFTBUKKIT);

            /*
             * now, convert the player to craftplayer using reflection, this is
             * the same thing as doing ((CraftPlayer)player)
             */
            Object converted = craftPlayer.cast(player);

            /*
             * using the converted class which sould be a CraftPlayer instance,
             * try to find the getHandle method
             */
            Method handle = converted.getClass().getMethod("getHandle");

            /*
             * now we just have to call this method, is the same thing as doing
             * the ((CraftPlayer)player).getHandle();
             */
            Object entityPlayer;
            entityPlayer = handle.invoke(converted);

            Field localeField = entityPlayer.getClass().getField("locale");
            locale = (String) localeField.get(entityPlayer);

            return locale;

        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | NoSuchFieldException ex) {
            Logger.getLogger(TitlePlayerUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return locale;
    }
}
