/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.titles.listeners;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.titles.Title;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class FirstPlayerJoinTitle implements Listener {

    private LOTC plugin;

    public FirstPlayerJoinTitle(LOTC instance) {
        this.plugin = instance;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        // Check permission
        // if (player.hasPermission("titlemotd.show")) {         
        if (!player.hasPlayedBefore()) {
            Title title = new Title(this.plugin.getTitleManager().getFirstJoinTitle());
            title.setTitle(title.getTitle().replace("%player%", player.getName()));
            title.setSubtitle(title.getSubtitle().replace("%player%", player.getName()));
            if (this.plugin.getFileManager().getConfiguration().getBoolean("Title.FirstJoin", "BroadCast")) {
                for (Player target : Bukkit.getOnlinePlayers()) {
                    title.send(target);
                }
            } else {
                title.send(player);
            }
        }
        //}
    }
}
