/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.gui;

import java.util.ArrayList;
import org.bukkit.plugin.java.JavaPlugin;

public class GuiManager {

    private ArrayList<InventoryGUI> iGUI = new ArrayList<>();
    private ArrayList<ChestGUI> cGUI = new ArrayList<>();
    
    public GuiManager() {
        
    }
    
    public InventoryGUI getInventoryGUI(JavaPlugin jp) {
        InventoryGUI gui = new InventoryGUI();
        this.iGUI.add(gui);
        return gui;
    }
    
    public ChestGUI getChestGUI(JavaPlugin jp) {
        ChestGUI gui = new ChestGUI();
        this.cGUI.add(gui);
        return gui;
    }
    
}
