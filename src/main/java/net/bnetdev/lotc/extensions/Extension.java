/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.extensions;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.actionbar.ActionBarManager;
import net.bnetdev.lotc.commands.AbstractCommand;
import net.bnetdev.lotc.conversation.ConversationManager;
import net.bnetdev.lotc.conversation.CustomConversation;
import net.bnetdev.lotc.database.Database;
import net.bnetdev.lotc.database.DatabaseManager;
import net.bnetdev.lotc.database.DatabaseType;
import net.bnetdev.lotc.files.Configuration;
import net.bnetdev.lotc.titles.Title;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Extension {

    private LOTC plugin;
    private JavaPlugin jp;
    private Plugin p;

    private boolean isRegistered = false;

    public Extension(LOTC instance, JavaPlugin e, boolean isRegistered) {
        this.plugin = instance;
        this.jp = e;
        this.isRegistered = isRegistered;
    }

    public Extension(LOTC instance, Plugin e) {
        this.plugin = instance;
        this.p = e;
    }

    public String getName() {
        if (this.jp != null) {
            return this.jp.getName();
        } else {
            return this.p.getName();
        }
    }

    public Object getInstance() {
        if (this.jp != null) {
            return this.getRegisteredInstance();
        } else {
            return this.getUnregisteredInstance();
        }
    }

    public boolean isRegistered() {
        return this.isRegistered;
    }

    public Configuration getConfiguration(String name) {
        try {
            return this.plugin.getFileManager().getConfiguration(this, name);
        } catch (IOException ex) {
            Logger.getLogger(Extension.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * This method can be used to obtain an instance of a MySQL or, SQLite
     * database. Unless otherwise stated, You do not have to include MySQL
     * parameters if you specified SQLite as the type. Though, you must provide
     * empty strings in place of the expected MySQL parameters.
     *
     * @param type - DatabaseType Enum [MySQL or, SQLite]
     * @param args[0] - IP Address for MySQL database
     * @param args[1] - Port for MySQL database
     * @param args[2] - Database (For SQLite use File.getAbsolutePath())
     * @param args[3] - Username for MySQL database
     * @param args[4] - Password for MySQL database
     * @param args[5] - Minimum Pool Connections for MySQL database
     * @param args[6] - Maximum Pool Connections for MySQL database
     * @param args[7] - Connection Timeoute for MySQL database
     *
     * @return Abstract Database, of type MySQL or, SQLite as specified. In
     * order to obtain a connection you must use the
     * #getDataSource()#getConnection() method. Verifying the type of database
     * is recommended before performing work with the database. For that
     * purpose, use Database instanceof MySQL/SQLite.
     */
    public Database getDatabase(DatabaseType type, String[] args) {
        return new DatabaseManager(this, type, args).getDatabase();
    }
    
    public Database getInternalDatabase() {
        return this.plugin.getDatabaseManager().getDatabase();
    }

    public File getDataFolder() {
        return new File(this.plugin.getDataFolder().getAbsolutePath().concat(File.separator).concat("Extensions").concat(File.separator).concat(this.getName()));
    }

    private JavaPlugin getRegisteredInstance() {
        return this.jp;
    }

    private Plugin getUnregisteredInstance() {
        return this.p;
    }

    public void hookMessage(String message) {
        this.plugin.getChatManager().log("[".concat(this.getName()).concat("] ").concat(message));
    }

    public void pluginMessage(String message) {
        if (this.getInstance() instanceof JavaPlugin) {
            this.jp.getLogger().info(message);
        } else {
            this.p.getLogger().info(message);
        }
    }

    public void registerCommand(AbstractCommand command) {
        this.plugin.getCommandManager().add(this, command);
    }

    public void unregisterCommand(AbstractCommand command) {
        this.plugin.getCommandManager().remove(this, command);
    }

    public void registerCommands(AbstractCommand[] commands) {
        this.plugin.getCommandManager().add(this, commands);
    }

    public void unregisterCommands(AbstractCommand[] commands) {
        this.plugin.getCommandManager().remove(this, commands);
    }

    public void sendActionBarMessage(Player p, String message, int time) {
        ActionBarManager.sendActionBar(p, message, time);
    }

    public void sendActionBarMessage(String message, int time) {
        ActionBarManager.sendActionBarToAllPlayers(message, time);
    }

    public Title getNewTitle() {
        return this.plugin.getTitleManager().getTitleInstance();
    }

    public CustomConversation getConversation(Prompt prompt, int timeout, Player p) {
        return this.plugin.getConversationManager().getNewConversation(prompt, timeout, p);
    }
    
    public List<CustomConversation> getConversations(Prompt prompt, int timeout, List<Player> players) {
        return this.plugin.getConversationManager().getNewConversations(prompt, timeout, players);
    }
    
    public ConversationManager getConversationManager() {
        return this.plugin.getConversationManager();
    }
    
}
