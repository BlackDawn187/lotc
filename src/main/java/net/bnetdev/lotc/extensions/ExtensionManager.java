/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.extensions;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bnetdev.lotc.LOTC;
import org.bukkit.Bukkit;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.UnknownDependencyException;
import org.bukkit.plugin.java.JavaPlugin;

public class ExtensionManager {

    private final LOTC plugin;
    private final ArrayList<Extension> extensions = new ArrayList<>();

    public ExtensionManager(LOTC instance) {
        this.plugin = instance;
    }

    public int add(JavaPlugin plugin, String version) {
        if (this.versionCheck(version) == 1) {
            return 1;
        }
        
        if (version.equalsIgnoreCase(this.plugin.getDescription().getVersion())) {
            for (Extension e : this.extensions) {
                if (e.getName().equalsIgnoreCase(plugin.getName())) {
                    return 1;
                }
            }

            this.extensions.add(new Extension(this.plugin, plugin, true));
            this.plugin.getChatManager().log("Hook: ".concat(plugin.getName()).concat(" Enabled"));
            return 0;
        } else {
            this.plugin.getChatManager().log("Error: Version mismatch, please update ".concat(plugin.getName()).concat(" to reference the current framework version."));
            this.plugin.getChatManager().log("Error: Current Framework Version: ".concat(this.plugin.getDescription().getVersion()).concat(" || Requested Version: ").concat(version));
            return 1;
        }
    }

    public int rem(JavaPlugin plugin) {
        for (Extension e : this.extensions) {
            if (e.getName().equalsIgnoreCase(plugin.getName())) {
                this.extensions.remove(e);
                return 0;
            }
        }
        return 1;
    }

    public void load() {
        File exts = new File(this.plugin.getDataFolder().getAbsolutePath().concat(File.separator).concat("Extensions").concat(File.separator));
        for (String e : exts.list()) {
            if (e.contains(".jar")) {
                try {
                    this.plugin.getChatManager().log("Loading Extension: ".concat(e.split("\\.")[0]));
                    Plugin p = Bukkit.getPluginManager().loadPlugin(new File(this.plugin.getDataFolder().getAbsolutePath().concat(File.separator).concat("Extensions").concat(File.separator).concat(e)));
                    Bukkit.getPluginManager().enablePlugin(p);
                } catch (InvalidPluginException | InvalidDescriptionException | UnknownDependencyException ex) {
                    Logger.getLogger(ExtensionManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Extension get(String name) {
        for (Extension e : this.extensions) {
            if (e.getName().equalsIgnoreCase(name)) {
                return e;
            }
        }

        Plugin p = Bukkit.getPluginManager().getPlugin(name);
        if (p != null) {
            return new Extension(this.plugin, Bukkit.getPluginManager().getPlugin(name));
        } else {
            throw new NullPointerException("The extension you requested is not available.");
        }

    }

    private int versionCheck(String version) {
        String[] temp = this.plugin.getDescription().getVersion().split("\\.");
        if (temp.length < 3) {
            return 1;
        }

        String[] temp2 = version.split("\\.");
        if (temp2.length < 3) {
            this.plugin.getChatManager().log("Error: Version Format Invalid.");
            this.plugin.getChatManager().log("Error: Current Framework Version: ".concat(this.plugin.getDescription().getVersion()).concat(" || Requested Version: ").concat(version));
            return 1;
        }

        if (Integer.valueOf(temp2[0]) > Integer.valueOf(temp[0])) {
            this.plugin.getChatManager().log("Error: Major Version: ".concat(temp2[0]).concat(" does not exist."));
            return 1;
        }

        if (Integer.valueOf(temp2[1]) > Integer.valueOf(temp[1])) {
            this.plugin.getChatManager().log("Error: Minor Version: ".concat(temp2[0]).concat(" does not exist."));
            return 1;
        }

        if (Integer.valueOf(temp2[2]) > Integer.valueOf(temp[2])) {
            this.plugin.getChatManager().log("Error: Build Version: ".concat(temp2[0]).concat(" does not exist."));
            return 1;
        }
        
        return 0;
    }

}
