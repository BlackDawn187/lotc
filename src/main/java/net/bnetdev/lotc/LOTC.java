/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc;

import net.bnetdev.lotc.actionbar.ActionBarManager;
import net.bnetdev.lotc.gui.GuiManager;
import net.bnetdev.lotc.chat.ChatManager;
import net.bnetdev.lotc.commands.CommandManager;
import net.bnetdev.lotc.conversation.ConversationManager;
import net.bnetdev.lotc.database.DatabaseManager;
import net.bnetdev.lotc.economy.EconomyManager;
import net.bnetdev.lotc.extensions.ExtensionManager;
import net.bnetdev.lotc.files.FileManager;
import net.bnetdev.lotc.listeners.ListenerManager;
import net.bnetdev.lotc.minechat.MineChatManager;
import net.bnetdev.lotc.titles.TitleManager;
import org.bukkit.plugin.java.JavaPlugin;

public class LOTC extends JavaPlugin {

    private static LOTC instance;

    private ExtensionManager eManager;
    private FileManager fManager;
    private ChatManager cManager;
    
    private TitleManager tManager;
    private ActionBarManager abManager;
    private MineChatManager mcManager;
    private CommandManager coManager;
    private GuiManager gManager;
    private ListenerManager lManager;
    private EconomyManager ecManager;
    private DatabaseManager dManager;
    private ConversationManager convManager;

    @Override
    public void onEnable() {
        instance = this;

        this.cManager = new ChatManager(this);
        this.fManager = new FileManager(this);
        this.dManager = new DatabaseManager(this);
        this.cManager.init();
        this.lManager = new ListenerManager(this);
        
        this.tManager = new TitleManager(this);
        this.abManager = new ActionBarManager(this);
        this.mcManager = new MineChatManager(this);
        this.gManager = new GuiManager();
        this.coManager = new CommandManager(this);
        this.ecManager = new EconomyManager(this);
        this.convManager = new ConversationManager(this);
        this.eManager = new ExtensionManager(this);
        this.eManager.load();
    }

    @Override
    public void onDisable() {

    }

    public FileManager getFileManager() {
        return this.fManager;
    }

    public ExtensionManager getExtensionManager() {
        return this.eManager;
    }

    public TitleManager getTitleManager() {
        return this.tManager;
    }
    
    public ActionBarManager getActionBarManager() {
        return this.abManager;
    }
    
    public ConversationManager getConversationManager() {
        return this.convManager;
    }
    
    public MineChatManager getMineChatManager() {
        return this.mcManager;
    }
    
    public ChatManager getChatManager() {
        return this.cManager;
    }

    public ListenerManager getListenerManager() {
        return this.lManager;
    }
        
    public GuiManager getGuiManager() {
        return this.gManager;
    }
    
    public DatabaseManager getDatabaseManager() {
        return this.dManager;
    }

    public CommandManager getCommandManager() {
        return this.coManager;
    }

    public EconomyManager getEconomyManager() {
        return this.ecManager;
    }

    public static LOTC getInstance() {
        return instance;
    }

}
