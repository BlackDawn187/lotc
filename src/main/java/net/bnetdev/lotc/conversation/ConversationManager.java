/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.conversation;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.bnetdev.lotc.LOTC;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

public class ConversationManager {

    private LOTC plugin;

    public ConversationManager(LOTC instance) {
        this.plugin = instance;
    }

    private ConversationFactory getNewFactory() {
        return new ConversationFactory(this.plugin);
    }
    
    public CustomConversation getNewConversation(Prompt prompt, int timeout, Player p) {
        return new CustomConversation(prompt, timeout, p);
    }
    
    public List<CustomConversation> getNewConversations(Prompt prompt, int timeout, List<Player> players) {
        List<CustomConversation> temp = new ArrayList<>();
        for (Player p : players) {
            temp.add(new CustomConversation(prompt, timeout, p));
        }
        
        return temp;
    }
       
    public void sendAll(List<CustomConversation> conversations) {
        for (CustomConversation c : conversations) {
            c.send();
        }
    }
    
    public void send(List<CustomConversation> conversations, UUID uuid) {
        for (CustomConversation c : conversations) {
            if (c.getPlayer().getUniqueId().equals(uuid)) {
                c.send();
            }
        }
    }

}
