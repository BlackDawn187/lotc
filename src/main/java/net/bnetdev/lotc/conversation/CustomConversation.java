/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.conversation;

import net.bnetdev.lotc.LOTC;
import org.bukkit.ChatColor;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.ConversationPrefix;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

public class CustomConversation {

    private final static LOTC plugin = LOTC.getInstance();
    private final static ConversationFactory cFactory = new ConversationFactory(plugin);
    private Conversation conversation;
    private Prompt cPrompt;
    private Player cPlayer;
    private int cTimeout;

    public CustomConversation(Prompt prompt, int timeout, Player p) {
        this.cPrompt = prompt;
        this.cTimeout = timeout;
        this.cPlayer = p;

        cFactory.withFirstPrompt(this.cPrompt).withTimeout(this.cTimeout).withPrefix(new ConversationPrefix() {

            @Override
            public String getPrefix(ConversationContext context) {
                return ChatColor.translateAlternateColorCodes('&', plugin.getFileManager().getConfiguration().getString("Plugin Chat Tag", "Tag"));
            }
        }).thatExcludesNonPlayersWithMessage(plugin.getFileManager().getConfiguration().getString("Plugin Chat Tag", "Tag").concat(" This prompt is not available via Console.")).withLocalEcho(false);

        this.conversation = cFactory.buildConversation(p);
    }

    public void send() {
        this.conversation.begin();
    }

    public Player getPlayer() {
        return this.cPlayer;
    }

    public CustomConversation updateExistingPlayer(Player p) {
        if (this.cPlayer == p) {
            return this;
        } else {
            return new CustomConversation(this.cPrompt, this.cTimeout, p);
        }
    }

    public CustomConversation updateExistingPrompt(Prompt prompt) {
        if (this.cPrompt.equals(prompt)) {
            return this;
        } else {
            return new CustomConversation(prompt, this.cTimeout, this.cPlayer);
        }
    }
    
    public CustomConversation updateExistingTimeout(int timeout) {
        if (this.cTimeout == timeout) {
            return this;
        } else {
            return new CustomConversation(this.cPrompt, timeout, this.cPlayer);
        } 
    }

}
